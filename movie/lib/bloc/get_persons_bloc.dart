import 'package:flutter/cupertino.dart';
import 'package:movie/model/person_response.dart';
import 'package:movie/repository/repository.dart';
import 'package:rxdart/subjects.dart';

class PersonsListBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<PersonResponse> _subject =
      BehaviorSubject<PersonResponse>();

  getMovies() async {
    PersonResponse response = await _repository.getPersons();
    _subject.sink.add(response);
  }

  void drainSteam() {
    _subject.value == null;
  }
  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }


  BehaviorSubject<PersonResponse> get subject => _subject;
}

final personsBloc = PersonsListBloc();
