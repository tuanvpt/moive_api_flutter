import 'package:dio/dio.dart';
import 'package:movie/model/genre_response.dart';
import 'package:movie/model/movie_response.dart';
import 'package:movie/model/person_response.dart';

class MovieRepository {
  final String apiKey = "63e2a9c698c55e8514bed6801145816a";
  static String mainUrl = "https://api.themoviedb.org/3";
  final Dio _dio = Dio();


  var getUpComingApi = '$mainUrl/movie/upcoming';
  var getPopularMoviesApi = '$mainUrl/movie/popular';
  var getTopRatedMoviesApi = '$mainUrl/movie/top_rated';
  var getNowPlayingMoviesApi = '$mainUrl/movie/now_playing';

  var getPopularUrl = '$mainUrl/movie/top_rated';
  var getMoviesUrl = '$mainUrl/discover/movie';
  var getPlayingUrl = '$mainUrl/movie/now_playing';
  var getGenresUrl = '$mainUrl/genre/movie/list';
  var getPersonUrl = '$mainUrl/trending/person/week';

  //Movies
  Future<MovieResponse> getMovies() async {
    var params = {"api_key": apiKey, "language": "en-Us", "page:": 1};

    try {
      Response response =
          await _dio.get(getPopularUrl, queryParameters: params);
      print("Movies info: ${response.data} ");
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occurred: $error stacktrace: $stacktrace");
      return MovieResponse.withError("$error");
    }
  }

  //Play
  Future<MovieResponse> getPlayingMovies() async {
    var params = {"api_key": apiKey, "language": "en-Us", "page:": 1};

    try {
      Response response =
          await _dio.get(getPlayingUrl, queryParameters: params);
      print("Playing Movie info: ${response.data} ");
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occurred: $error stacktrace: $stacktrace");
      return MovieResponse.withError("$error");
    }
  }

  //Genres
  Future<GenreResponse> getGenres() async {
    var params = {"api_key": apiKey, "language": "en-Us", "page:": 1};

    try {
      Response response = await _dio.get(getGenresUrl, queryParameters: params);
      print("Genre info: ${response.data} ");
      return GenreResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occurred: $error stacktrace: $stacktrace");
      return GenreResponse.withError("$error");
    }
  }

  //Person
  Future<PersonResponse> getPersons() async {
    var params = {"api_key": apiKey};

    try {
      Response response = await _dio.get(getGenresUrl, queryParameters: params);
      print("Person info: ${response.data} ");
      return PersonResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occurred: $error stacktrace: $stacktrace");
      return PersonResponse.withError("$error");
    }
  }

  //Person
  Future<MovieResponse> getMovieByGenre(int id) async {
    var params = {
      "api_key": apiKey,
      "language": "en-Us",
      "page:": 1,
      "with_genres": id
    };

    try {
      Response response = await _dio.get(getMoviesUrl, queryParameters: params);
      print("Person info: ${response.data} ");
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occurred: $error stacktrace: $stacktrace");
      return MovieResponse.withError("$error");
    }
  }
}
