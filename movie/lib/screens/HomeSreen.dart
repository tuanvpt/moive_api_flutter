import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:movie/style/theme.dart' as Style;
import 'package:movie/widgets/now_playing.dart';

class HomeSreen extends StatefulWidget {
  const HomeSreen({Key? key}) : super(key: key);

  @override
  _HomeSreenState createState() => _HomeSreenState();
}

class _HomeSreenState extends State<HomeSreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      appBar: AppBar(
        backgroundColor: Style.Colors.mainColor,
        centerTitle: true,
        leading: const Icon(
          EvaIcons.menu2Outline,
          color: Colors.white,
        ),
        title: const Text('Movie API'),
        actions: const [
          IconButton(
              onPressed: null, icon: Icon(EvaIcons.search, color: Colors.white))
        ],
      ),
      body: ListView(
        children: const [
          NowPlaying(),
        ],
      ),
    );
  }
}
